package ben.upsilon;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NestedList extends LinearLayout {

	private Map<Integer, List<Item>> kvs;
	private List<Item> groups;
	private LayoutInflater inflater;

	public interface OnChildItemClickListener {
		void onChildItemClick(View view, Item item);
	}

	private OnChildItemClickListener onChildItemClickListener;

	public NestedList(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflater = LayoutInflater.from(context);

	}

	public void setItems(Map<Integer, List<Item>> kvs) {
		this.kvs = kvs;
		notifyData();
	}

	private void notifyData() {
		groups = kvs.get(0);
		for (Item item : groups) {
			LinearLayout ll = (LinearLayout) inflater.inflate(
					R.layout.group_item, null);
			TextView tv = (TextView) ll.findViewById(R.id.m_group_item_text);
			tv.setText(item.getName());
			tv.setId(item.getID());
			tv.setOnClickListener(new Expand());
			addView(ll);
		}
	}

	/**
	 * 
	 * 节点关闭
	 * 
	 */
	class Collapse implements OnClickListener {

		@Override
		public void onClick(View v) {

			View _v = (View) v.getParent();
			int index = indexOfChild(_v);
			LinearLayout ll = (LinearLayout) inflater.inflate(
					R.layout.group_item, null);
			TextView tv = (TextView) ll.findViewById(R.id.m_group_item_text);
			tv.setText(groups.get(index).getName());
			tv.setId(groups.get(index).getID());
			tv.setTag(groups.get(index));
			tv.setOnClickListener(new Expand());
			removeView(_v);
			addView(ll, index);

		}
	}

	/**
	 * 
	 * 节点展开
	 * 
	 */
	class Expand implements OnClickListener {

		@Override
		public void onClick(View v) {
			CheckBox _v = (CheckBox) v;

			LinearLayout childLayout = (LinearLayout) inflater.inflate(
					R.layout.group_title_child_list, null);
			CheckBox gt = (CheckBox) childLayout.findViewById(R.id.group_title);
			gt.setText(_v.getText());
			gt.setChecked(true);
			gt.setOnClickListener(new Collapse());
			LinearLayout childlist = (LinearLayout) childLayout
					.findViewById(R.id.child_list);
			for (Item item : ItemParser.itemMap.get(_v.getId())) {
				TextView c = (TextView) inflater.inflate(R.layout.sub_item,
						null);
				c.setText(item.getName());
				c.setId(item.getID());
				c.setTag(item);
				c.setOnClickListener(new ItemClick());
				childlist.addView(c);
			}

			int index = indexOfChild((View) v.getParent());

			removeView((View) v.getParent());
			addView(childLayout, index);

		}

	}

	public void setOnChildItemClickListener(
			OnChildItemClickListener onChildItemClickListener) {
		this.onChildItemClickListener = onChildItemClickListener;
	}

	/**
	 * 
	 * item点击逻辑
	 * 
	 */
	class ItemClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (onChildItemClickListener != null)
				onChildItemClickListener.onChildItemClick(v, (Item) v.getTag());
		}

	}

}
