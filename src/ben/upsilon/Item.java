package ben.upsilon;

/**
 * 节点值对象
 * 
 * @author ben.upsilon@gmail.com
 * 
 */
public class Item {
	// 当前节点id
	private int _id = -1;
	// 父节点id
	private int _parent_id = -1;
	// 当前节点的名字
	private String _name = "";
	// 当前节点的数据
	private String _data = "";

	/**
	 * 构造新节点对象
	 * 
	 * @param id
	 *            当前节点id
	 * @param pid
	 *            父节点id
	 * @param name
	 *            当前节点名字
	 * @param data
	 *            当前节点数据
	 */
	public Item(int id, int pid, String name, String data) {
		_id = id;
		_parent_id = pid;
		_name = name;
		_data = data;
	}

	/**
	 * 返回当前节点id
	 * 
	 * @return 当前节点id
	 */
	public int getID() {
		return _id;
	}

	/**
	 * 返回父节点id
	 * 
	 * @return 父节点id
	 */
	public int getParentID() {
		return _parent_id;
	}

	/**
	 * 返回当前节点的名字
	 * 
	 * @return 当前节点名字
	 */
	public String getName() {
		return _name;
	}

	/**
	 * 返回当前节点的数据
	 * 
	 * @return 当前节点的数据
	 */
	public String getData() {
		return _data;
	}

	@Override
	public String toString() {
		return "Item [_id=" + _id + ", _parent_id=" + _parent_id + ", _name="
				+ _name + ", _data=" + _data + "]";
	}
 

}
