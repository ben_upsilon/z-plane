package ben.upsilon;

import java.util.LinkedList;

import ben.upsilon.List2Adapter.Meta;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Html.TagHandler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * 二级列表的实现
 * 
 * @author ben.upsilon@gmail.com
 * 
 */
public class List2 extends AdapterView<List2Adapter> {

	public static interface OnGroupItemClickListener {
		public void onGroupItemClick(int groupPos, long id);
	}

	public static interface OnChildItemClickListener {
		public void onChildItemClick(int groupPos, int childPos, long id);
	}

	private final String LogTag = "ben.upsilon.list2";
	/**
	 * group点击
	 */
	public OnGroupItemClickListener onGroupItemClickListener;

	public OnChildItemClickListener onChildItemClickListener;
	/**
	 * group item 背景圖片
	 */
	private Drawable groupItemBackground;
	/**
	 * 
	 */

	private int childItemPaddingTop;
	private int childItemPaddingLeft;
	private int childItemPaddingRight;
	private int childItemPaddingBottom;
	private Drawable childItemDivider;
	private Drawable childLayoutBackground;
	private int childLayoutPaddingTop;
	private int childLayoutPaddingLeft;
	private int childLayoutPaddingRight;
	private int childLayoutPaddingBottom;

	/** Children added with this layout mode will be added below the last child */
	private static final int LAYOUT_MODE_BELOW = 0;

	/** Children added with this layout mode will be added above the first child */
	private static final int LAYOUT_MODE_ABOVE = 1;

	/** 用户触摸状态为重置 */
	private static final int TOUCH_STATE_RESTING = 0;

	/** 用户触摸状态为点击 */
	private static final int TOUCH_STATE_CLICK = 1;

	/** 用户触摸状态为滚动 */
	private static final int TOUCH_STATE_SCROLL = 2;

	/** 当前触摸状态 */
	private int mTouchState = TOUCH_STATE_RESTING;

	private List2Adapter mAdapter;

	private Paint font;
	
	private ListView lv;

	private int mLastItemPosition, mFirstItemPosition;
	private int mListTop, mListTopOffset, mTouchStartY, mTouchStartX,
			mListTopStart;

	private final LinkedList<View> mCachedItemViews = new LinkedList<View>();

	public List2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs,
				ben.upsilon.R.styleable.list2, defStyle, 0);

		groupItemBackground = a
				.getDrawable(ben.upsilon.R.styleable.list2_group_item_background);

		a.recycle();

		font = new Paint(Paint.ANTI_ALIAS_FLAG);
		font.setColor(Color.BLUE);
		font.setTextSize(30);
		font.setFakeBoldText(true);
	}

	public List2(Context context, AttributeSet attrs) {
		this(context, attrs, -1);
	}

	public List2(Context context) {
		this(context, null);
	}
/**
 * 在每个item右边加个图片
 */
	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		Log.d(LogTag, "dispatchDraw");
		int imgHeight = groupItemBackground.getIntrinsicHeight();
		int imgWidth = groupItemBackground.getIntrinsicWidth();
		for (int i = 0; i < getChildCount(); i++) {
			View v = getChildAt(i);

			groupItemBackground.setBounds(getWidth() - imgWidth, v.getTop(),
					getWidth(), v.getTop() + imgHeight);
			groupItemBackground.draw(canvas);
		}

	}

	/**
	 * 一开始就调用onlayout,用来解决adapter里面item对象的显示
	 */
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		Log.d(LogTag, "onLayout");
		super.onLayout(changed, left, top, right, bottom);
		if (getChildCount() == 0) {
			Log.d(LogTag, "onLayout getChildCount : "+getChildCount());
			//因为没childs,所以重置postion为-1,并开始填充,
			mLastItemPosition = -1;
			fillListDown(mListTop, 0);
		} else {
			Log.d(LogTag, "onLayout getChildCount : "+getChildCount());
			final int offset = mListTop + mListTopOffset
					- getChildAt(0).getTop();
			removeNonVisibleViews(offset);
			fillList(offset);
		}

		positionItems();
		invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.d(LogTag, event.toString());
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			startTouch(event);
			break;

		case MotionEvent.ACTION_MOVE:
			if (mTouchState == TOUCH_STATE_CLICK) {
				startScrollIfNeeded(event);
			}
			if (mTouchState == TOUCH_STATE_SCROLL) {
				scrollList((int) event.getY() - mTouchStartY);
			}
			break;

		case MotionEvent.ACTION_UP:
			if (mTouchState == TOUCH_STATE_CLICK) {
				clickChildAt((int) event.getX(), (int) event.getY());
			}
			endTouch();
			break;

		default:
			endTouch();
			break;
		}
		return true;

	}

	private Rect mRect;

	private int getContainingChildIndex(final int x, final int y) {
		if (mRect == null) {
			mRect = new Rect();
		}
		for (int index = 0; index < getChildCount(); index++) {
			getChildAt(index).getHitRect(mRect);
			if (mRect.contains(x, y)) {
				return index;
			}
		}
		return -1;
	}

	@Override
	public boolean performItemClick(View view, int position, long id) {
		Meta m = mAdapter.getMeta(position);
		if (m.isExpanded) {
			mAdapter.collapseGroup(m);
			if (onGroupItemClickListener != null)
				onGroupItemClickListener.onGroupItemClick(m.position, id);

		} else {
			mAdapter.expandGroup(m);
		}
		return true;
	}

	/**
	 * 获取点击所对应的子项目
	 * 
	 * @param x
	 * @param y
	 */
	private void clickChildAt(final int x, final int y) {
		final int index = getContainingChildIndex(x, y);
		if (index != -1) {
			final View itemView = getChildAt(index);
			final int position = mFirstItemPosition + index;
			final long id = mAdapter.getItemId(position);

			performItemClick(itemView, position, id);
		}
	}

	private void scrollList(final int scrolledDistance) {
		mListTop = mListTopStart + scrolledDistance;
		requestLayout();
	}

	/** 不能老触发啊啊啊 */
	private static final int TOUCH_SCROLL_THRESHOLD = 10;

	/**
	 * 如果需要滚动的话
	 * 
	 * @param event
	 * @return
	 */
	private boolean startScrollIfNeeded(final MotionEvent event) {
		final int xPos = (int) event.getX();
		final int yPos = (int) event.getY();
		if (xPos < mTouchStartX - TOUCH_SCROLL_THRESHOLD
				|| xPos > mTouchStartX + TOUCH_SCROLL_THRESHOLD
				|| yPos < mTouchStartY - TOUCH_SCROLL_THRESHOLD
				|| yPos > mTouchStartY + TOUCH_SCROLL_THRESHOLD) {
			// 滚滚滚滚
			mTouchState = TOUCH_STATE_SCROLL;
			return true;
		}
		return false;
	}

	private void startTouch(final MotionEvent event) {
		// 開始觸摸的初始化
		mTouchStartX = (int) event.getX();
		mTouchStartY = (int) event.getY();
		mListTopStart = getChildAt(0).getTop() - mListTopOffset;

		// 不知道是点击还是滚动,但默认是点击
		mTouchState = TOUCH_STATE_CLICK;
	}

	/**
	 * 結束觸摸
	 */
	private void endTouch() {
		// 重置触摸状态
		mTouchState = TOUCH_STATE_RESTING;
	}

	/**
	 * 添加并计算宽度
	 * 
	 * @param child
	 *            子對象
	 * @param layoutMode
	 *            从上面加入还是从下面加入
	 */
	private void addAndMeasureChild(final View child, final int layoutMode) {
		//不用解释了...
		LayoutParams params = child.getLayoutParams();
		if (params == null) {
			params = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
		}
		//看索引到底add到哪儿位置
		final int index = layoutMode == LAYOUT_MODE_ABOVE ? 0 : -1;
		//然后没然后了...
		addViewInLayout(child, index, params, true);
		//然后让item给个宽度....填满到上一级的最大宽度
		final int itemWidth = getWidth();
		child.measure(MeasureSpec.EXACTLY | itemWidth, MeasureSpec.UNSPECIFIED);
	}

	/**
	 * 确定所有item的位置
	 */
	private void positionItems() {
		int top = mListTop + mListTopOffset;

		for (int index = 0; index < getChildCount(); index++) {
			final View child = getChildAt(index);

			final int width = child.getMeasuredWidth();
			final int height = child.getMeasuredHeight();
			final int left = (getWidth() - width) / 2;

			child.layout(left, top, left + width, top + height);
			top += height;
		}

	}

	/**
	 * 移除看不到的view
	 * @param offset
	 */
	private void removeNonVisibleViews(final int offset) {
		int childCount = getChildCount();
		System.out.println(childCount);
		if (mLastItemPosition != mAdapter.getCount() - 1 && childCount > 1) {
			View firstChild = getChildAt(0);
			while (firstChild != null && firstChild.getBottom() + offset < 0) {

				removeViewInLayout(firstChild);
				childCount--;
				mCachedItemViews.addLast(firstChild);
				mFirstItemPosition++;

				mListTopOffset += firstChild.getMeasuredHeight();

				if (childCount > 1) {
					firstChild = getChildAt(0);
				} else {
					firstChild = null;
				}
			}
		}

		if (mFirstItemPosition != 0 && childCount > 1) {
			View lastChild = getChildAt(childCount - 1);
			while (lastChild != null
					&& lastChild.getTop() + offset > getHeight()) {
				removeViewInLayout(lastChild);
				childCount--;
				mCachedItemViews.addLast(lastChild);
				mLastItemPosition--;

				if (childCount > 1) {
					lastChild = getChildAt(childCount - 1);
				} else {
					lastChild = null;
				}
			}
		}
	}

	/**
	 * 填充item到可視區
	 * 
	 * @param offset 偏移值
	 * 
	 */
	private void fillList(final int offset) {
		final int bottomEdge = getChildAt(getChildCount() - 1).getBottom();// 下面邊界
		fillListDown(bottomEdge, offset);

		final int topEdge = getChildAt(0).getTop();// 上面邊界
		fillListUp(topEdge, offset);
	}

	private void fillListUp(int topEdge, final int offset) {
		Log.d(LogTag, "fillListUp");
		while (topEdge + offset > 0 && mFirstItemPosition > 0) {
			mFirstItemPosition--;
			final View newTopCild = mAdapter.getView(mFirstItemPosition,
					getCachedView(), this);
			addAndMeasureChild(newTopCild, LAYOUT_MODE_ABOVE);
			final int childHeight = newTopCild.getMeasuredHeight();
			topEdge -= childHeight;	
			//更新offset
			mListTopOffset -= childHeight;
		}
	}

	private void fillListDown(int bottomEdge, final int offset) {
		Log.d(LogTag, "fillListDown");
		//循环....高度不超过可看的范围,,还有items个数..
		while (bottomEdge + offset < getHeight()
				&& mLastItemPosition < mAdapter.getCount()-1) {
			mLastItemPosition++;
			//获取child view
			final View newBottomchild = mAdapter.getView(mLastItemPosition,
					getCachedView(), this);
			//加入并计算
			addAndMeasureChild(newBottomchild, LAYOUT_MODE_BELOW);
			//边界叠加
			bottomEdge += newBottomchild.getMeasuredHeight();
		}
	}

	private View getCachedView() {
		if (mCachedItemViews.size() != 0) {
			return mCachedItemViews.removeFirst();
		}
		return null;
	}

	@Override
	public List2Adapter getAdapter() {
		return mAdapter;
	}

	@Override
	public void setAdapter(List2Adapter adapter) {
		mAdapter = adapter;
		requestLayout();
	}

	@Override
	public View getSelectedView() {
		return null;
	}

	@Override
	public void setSelection(int position) {

	}

}
