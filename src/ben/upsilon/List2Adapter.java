package ben.upsilon;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import ben.upsilon.List2Adapter.Meta;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class List2Adapter extends BaseAdapter {
	private final String LogTag = "ben.upsilon.list2adapter";
	private Map<Integer, List<Item>> _items;

	private List<Meta> metas = new ArrayList<List2Adapter.Meta>();
	private List<Item> toShowlists = new ArrayList<Item>();

	private LayoutInflater inflater;

	public List2Adapter(Context context, Map<Integer, List<Item>> __items) {
		inflater = LayoutInflater.from(context);
		this._items = __items;
		toShowlists.addAll(_items.get(0));
		for (int i = 0; i < toShowlists.size(); i++) {
			Meta m = new Meta();
			m.isExpanded = false;
			m.isGroup = true;
			m.position = toShowlists.get(i).getID();
			metas.add(m);
		}

	}

	@Override
	public int getCount() {
		Log.d(LogTag,
				String.format("to show list size :%d", toShowlists.size()));
		return toShowlists.size();
	}

	@Override
	public Item getItem(int position) {
		Log.d(LogTag,
				String.format("to show list getItem :%s",
						toShowlists.get(position)));

		return toShowlists.get(position);
	}

	@Override
	public long getItemId(int position) {
		Log.d(LogTag, String.format("to show list getItemId :%s", position));
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView v = (TextView) convertView;
		if (v == null) {
			v = (TextView) inflater.inflate(
					android.R.layout.simple_list_item_1, null);
		}
		v.setText(getItem(position).getName());

		return v;
	}

	class Meta {
		boolean isGroup;
		int position;
		boolean isExpanded;

		@Override
		public String toString() {
			return "Meta [isGroup=" + isGroup + ", position=" + position
					+ ", isExpanded=" + isExpanded + "]";
		}

	}

	public Meta getMeta(int position) {

		Meta m = null;
		if (metas.size() != 0) {
			m = metas.get(position);
		}
		if (m == null) {
			m = new Meta();
			m.position = position;
			metas.add(m);
		}
		return m;
	}

	public void collapseGroup(Meta m) {

		notifyDataSetChanged();
	}

	public void expandGroup(Meta m) {
		Log.d(LogTag, m.toString());
		toShowlists.addAll(_items.get(m.position));

		notifyDataSetChanged();
	}

}
