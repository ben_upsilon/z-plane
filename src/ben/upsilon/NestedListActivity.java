package ben.upsilon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class NestedListActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ExpandableListView view;
		AlertDialog a;

		List2 list2 = (List2) findViewById(R.id.list2);
		list2.setAdapter(new List2Adapter(this,new ItemParser(this)
				.getItemMap()));

		//
		// Map<Integer, List<Item>> kvs = new ItemParser(this).getItemMap();
		// NestedList list = (NestedList) findViewById(R.id.nested_list);
		// list.setItems(kvs);
		// list.setOnChildItemClickListener(new OnChildItemClickListener() {
		//
		// @Override
		// public void onChildItemClick(View view, Item item) {
		// System.out.println(item);
		// System.out.println(ItemParser.itemMap.get(item.getID()));
		// }
		// });
	}
}